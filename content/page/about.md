---
title: Acerca de
subtitle: Why you'd want to hang out with me
comments: false
---

Soy Noemi. Hice esta pagina por :

- Dar mas visibilidad a los casos de feminicidios  en Bolivia, con los datos del proyecto "CuantasMas".
- Liberar datos ayuda a generar nuevos proyectos.
- Dar uso a las herramientas de mapbox.



Para mas informacion sobre el proyecto puedes visitar las paginas de :
  - http://cuantasmas.org
  - http://feminicidiosbolivia.net/
### La historia
En Bolivia hay muchos casos de feminicidios sin resolver por diferentes factores.

Ni una menos.
